# Copyright 2008-2017 Wulf C. Krueger
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'openvpn-2.1_rc9.ebuild' from Gentoo, which is:
#       Copyright 1999-2008 Gentoo Foundation

#SCM_EXTERNAL_REFS="
#    vendor/cmocka:cmocka
#"

#SCM_SECONDARY_REPOSITORIES="cmocka"

#SCM_cmocka_REPOSITORY="https://git.cryptomilk.org/projects/cmocka.git"

require github [ user=OpenVPN tag=v${PV} force_git_clone=true ]
require systemd-service [ systemd_files=[ distro/systemd ] ]
require openrc-service [ openrc_confd_files=[ ${FILES}/openrc/confd ] ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

export_exlib_phases src_compile src_test src_install

SUMMARY="OpenVPN is a robust and highly configurable VPN daemon"
DESCRIPTION="
OpenVPN is a robust and highly configurable VPN (Virtual Private Network) daemon
which can be used to securely link two or more private networks using an encrypted
tunnel over the Internet. OpenVPN's principal strengths include wide cross-platform
portability, excellent stability, support for dynamic IP addresses and NAT, adaptive
link compression, single TCP/UDP port usage, a modular design that offloads most
crypto tasks to the OpenSSL library, and relatively easy installation that in most
cases doesn't require a special kernel module.
"
HOMEPAGE="https://${PN}.net"
#DOWNLOADS="https://swupdate.openvpn.net/community/releases/${PNV}.tar.xz"

BUGS_TO="philantrop@exherbo.org"
REMOTE_IDS="freecode:${PN}"

MY_UPSTREAM_DOCUMENTATION_BASE="${HOMEPAGE}/index.php/documentation"
UPSTREAM_CHANGELOG="${MY_UPSTREAM_DOCUMENTATION_BASE}/change-log/changelog-21.html"
UPSTREAM_RELEASE_NOTES="${MY_UPSTREAM_DOCUMENTATION_BASE}/release-notes.html"
UPSTREAM_DOCUMENTATION="${MY_UPSTREAM_DOCUMENTATION_BASE}/manuals/${PN}-21.html"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    examples
    lzo          [[ description = [ Enable the LZO compression ] ]]
    lz4          [[ description = [ Enable the LZ4 compression ] ]]
    pkcs11       [[ description = [ Support for smartcard authentication ] ]]
    systemd
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
    ( providers: net-tools iproute2 ) [[
        number-selected = exactly-one
        *description = [ Tool to manage routes ]
    ]]
"
DEPENDENCIES="
    build+run:
        sys-libs/pam[>=1.0.1]
        lzo? ( app-arch/lzo:2[>=2.02] )
        lz4? ( app-arch/lz4[>=1.7.1] )
        pkcs11? ( dev-libs/pkcs11-helper[>=1.06] )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl[>=0.9.6] )
    run:
        providers:iproute2? ( sys-apps/iproute2 )
        providers:net-tools? ( sys-apps/net-tools )
    post:
        group/openvpn
        user/openvpn
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-crypto
    --enable-plugins
    --enable-x509-alt-username
    --disable-selinux
    --with-crypto-library=openssl
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    lz4
    lzo
    pkcs11
    systemd
    "providers:iproute2 iproute2"
)

_compile_plugin() {
    local plugdir=${1:-"plugin does not exist"}
    if edo pushd src/plugins/"${plugdir}" ; then
        emake || die "making plugin ${plugdir} failed"
    else
        die "build failure in compile_plugin: ${plugdir}"
    fi
    edo popd
}

_install_plugin() {
    local plugdir=${1:-"plugin does not exist"}
    export DESTDIR="${IMAGE}"
    if edo pushd src/plugins/"${plugdir}" ; then
        emake -j1 install || die "installing plugin ${plugdir} failed"
    else
        die "install failure in install_plugin: ${plugdir}"
    fi
    edo popd
}

openvpn_src_compile() {
    default

    _compile_plugin auth-pam
    _compile_plugin down-root
}

openvpn_src_test() {
    esandbox disable_net

    emake check

    esandbox enable_net
}

openvpn_src_install() {
    default

    keepdir /etc/openvpn

    # Install some helper scripts
    exeinto /etc/openvpn
    doexe "${FILES}"/up.sh
    doexe "${FILES}"/down.sh

    # install examples, controlled by the respective useflag
    if option examples ; then
        insinto /usr/share/doc/${PNVR}/examples
        doins -r sample/* contrib
    fi

    # Install plugins
    _install_plugin auth-pam
    _install_plugin down-root

    # Install service files
    install_openrc_files
}

