# Copyright 2011 Alex ELsayed <eternaleye@gmail.com>
# Distributed under the terms of the GNU General Public License v2


SCM_REPOSITORY="https://code.videolan.org/videolan/libbluray.git"
SCM_EXTERNAL_REFS="
	contrib/libudfread:libudfread
"
SCM_SECONDARY_REPOSITORIES="
	libudfread
"
SCM_libudfread_REPOSITORY="git://git.videolan.org/libudfread.git"

require scm-git autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

SUMMARY="Open-source library designed for Blu-Ray Discs playback for media players, like VLC or MPlayer"
HOMEPAGE="https://www.videolan.org/developers/${PN}.html"


PLATFORMS="~amd64"


LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS="
    doc
    examples
"

DEPENDENCIES="
    build:
        dev-java/apache-ant
        virtual/pkg-config[>=0.9.0]
        doc? ( app-doc/doxygen[dot] )
        virtual/jdk:1.8
    build+run:
        dev-libs/libxml2:2.0[>=2.6]
        media-libs/fontconfig
        media-libs/freetype:2
    suggestion:
        virtual/libaacs [[ description = [ Decrypt encrypted blurays ] ]]
"

# Disable ps and pdf generation due to chronic latex shortage
# dlopen the crypto libs at runtime to avoid build-time automagic checks
DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-bdjava-jar
    --disable-doxygen-{pdf,ps}
    --disable-static
    --with-fontconfig
    --with-freetype
    --with-libxml2
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    "doc doxygen-doc"
    "doc doxygen-dot"
    examples
)

AT_M4DIR=( m4 )

src_configure() {
    export JDK_HOME="/usr/$(exhost --target)/lib/jdk"

    default
}

