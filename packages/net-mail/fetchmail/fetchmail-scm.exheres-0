# Copyright 2008, 2009, 2010 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'fetchmail-6.3.8.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation.

SCM_REPOSITORY="https://git.code.sf.net/p/fetchmail/git"
SCM_BRANCH="legacy_64"

require scm-git

SUMMARY="The legendary remote-mail retrieval and forwarding utility"
HOMEPAGE="http://www.${PN}.info/"

LICENCES="GPL-2 public-domain"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="tk
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        sys-devel/autoconf
        sys-devel/automake
        sys-devel/gettext
    build+run:
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
        tk? ( dev-lang/python:=[tk] )"

#DEFAULT_SRC_PREPARE_PATCHES=(
    # Permit build on SSLv3-disabled OpenSSL
#    "${FILES}"/no_sslv3.patch
#)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --with-ssl=/usr/$(exhost --target)
    --enable-nls
    --enable-RPA
    --enable-NTLM
    --enable-SDPS
    --without-gssapi
    --without-kerberos5
    --without-kerberos
    --without-hesiod
)
DEFAULT_SRC_COMPILE_PARAMS=( AR="${AR}" )
DEFAULT_SRC_INSTALL_EXTRA_DOCS=( FEATURES )

src_prepare() {
    default
    edo ./autogen.sh
}

src_configure() {
    # Configure automagically enables the tk interface unless PYTHON=:
    option tk || pythonexport PYTHON=:
    default
}

