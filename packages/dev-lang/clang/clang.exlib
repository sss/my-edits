# Copyright 2019 Marc-Antoine Perennou <keruspe@exherbo.org>
# Copyright 2018 Bjorn Pagen <bjornpagen@gmail.com>
# Copyright 2012 Elias Pipping <pipping@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require llvm-project [ projects=[ "${PN}" "${PN}-tools-extra" ] dependencies_label="post" check_target="check-clang" slotted=true rtlib=true stdlib=true ]
require alternatives

export_exlib_phases src_unpack src_prepare src_configure src_compile src_test src_install

SUMMARY="C language family frontend for LLVM"

DEPENDENCIES="
    build:
        dev-lang/perl:*
        sys-devel/flex
        doc? ( dev-python/Sphinx )
    build+run:
        dev-lang/llvm:${SLOT}
        dev-libs/libxml2:2.0[>=2.5.3]
    run:
        !dev-lang/clang:0[<5.0.1-r1] [[
            description = [ Old, unslotted clang not supported ]
            resolution = upgrade-blocked-before
        ]]
        !sys-devel/gcc:4.9[<4.9.2-r8] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
        !sys-devel/gcc:5.1[<5.2.0-r2] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
    post:
        app-admin/eclectic-clang[targets:*(-)?]
        providers:compiler-rt? ( dev-libs/compiler-rt:${SLOT} )
    suggestion:
        dev-libs/compiler-rt:${SLOT} [[
            description = [ Sanitizer runtimes for clang ]
        ]]
        sys-libs/openmp [[
            description = [ OpenMP runtime for clang ]
        ]]
"

# clang is a cross compiler and can compile for any target without a
# new binary being compiled for the target. this is only used for the
# creation of the symlinks for targets; /usr/${CHOST}/bin/${target}-clang
CROSS_COMPILE_TARGETS="
    aarch64-unknown-linux-gnueabi
    armv7-unknown-linux-gnueabi
    armv7-unknown-linux-gnueabihf
    i686-pc-linux-gnu
    i686-pc-linux-musl
    powerpc64-unknown-linux-gnu
    x86_64-pc-linux-gnu
    x86_64-pc-linux-musl
"

MYOPTIONS="
    doc python
    ( targets: ${CROSS_COMPILE_TARGETS} ) [[ number-selected = at-least-one ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/clang-scm-lit.cfg-use-PATH.patch
)

if ever at_least 10.0.0; then
    :
elif ever at_least 9.0.0; then
    DEFAULT_SRC_PREPARE_PATCHES+=(
        -p2 "${FILES}"/0001-clang-Exherbo-multiarch-ajustments.patch
        -p2 "${FILES}"/5e866e411caad4c4e17e7e0c67b06d28451e1bf2.patch
    )
else
    DEFAULT_SRC_PREPARE_PATCHES+=(
        -p2 "${FILES}"/clang-8-Driver-Set-correct-include-path-for-Exherbo.patch
    )
fi

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DLLVM_CONFIG:STRING=${LLVM_PREFIX}/bin/llvm-config

    # install clang to a slotted directory to prevent collisions with other clangs
    -DCMAKE_INSTALL_PREFIX:STRING=${LLVM_PREFIX}
    -DCMAKE_INSTALL_MANDIR:STRING=${LLVM_PREFIX}/share/man

    # TODO(compnerd) hidden inline visibility causes test tools to fail to build as a required
    # method is hidden; move the definition out of line, and export the interface
    -DSUPPORTS_FVISIBILITY_INLINES_HIDDEN_FLAG:BOOL=NO

    -DCLANG_BUILD_TOOLS:BOOL=ON
    -DCLANG_ENABLE_ARCMT:BOOL=ON
    -DCLANG_ENABLE_CLANGD:BOOL=ON
    -DCLANG_ENABLE_PROTO_FUZZER:BOOL=OFF
    -DCLANG_ENABLE_STATIC_ANALYZER:BOOL=ON
    -DCLANG_INSTALL_SCANBUILD:BOOL=ON
    -DCLANG_INSTALL_SCANVIEW:BOOL=ON
    -DCLANG_LINK_CLANG_DYLIB:BOOL=OFF
    -DCLANG_PLUGIN_SUPPORT:BOOL=ON
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DCLANG_INCLUDE_TESTS:BOOL=ON -DCLANG_INCLUDE_TESTS:BOOL=OFF'
)

clang_src_unpack() {
    llvm-project_src_unpack

    edo ln -s "${WORKBASE}"/llvm-project/clang-tools-extra "${CMAKE_SOURCE}"/tools/extra
}

clang_src_prepare() {
    edo pushd "${CMAKE_SOURCE}"

    if ! ever at_least 9.0.0; then
        # ld tests will fail because of Exherbo's non-standard path for ld
        edo rm test/Driver/{dyld-prefix.c,linux-ld.c,mips-cs.cpp}
    fi

    # Fails because we explicitely specify the default cxx stdlib
    edo rm test/Driver/{baremetal.cpp,fuzzer.c}

    # Fails because we explicitely specify the default rtlib
    edo rm test/Driver/wasm-toolchain.c{,pp}
    edo rm test/Headers/{arm-fp16-header.c,arm-neon-header.c}

    # The build system doesn't pick up that LLVM has zlib support when doing a standalone build
    edo rm test/Driver/nozlibcompress.c

    # Fix the use of dot
    edo sed -e 's/@DOT@//g' -i docs/doxygen.cfg.in
    default

    edo popd
}

clang_src_configure() {
    local args=()

    if option providers:libc++; then
        args+=( -DCLANG_DEFAULT_CXX_STDLIB:STRING="libc++" )
    else
        args+=( -DCLANG_DEFAULT_CXX_STDLIB:STRING="libstdc++" )
    fi

    if option providers:compiler-rt; then
        args+=( -DCLANG_DEFAULT_RTLIB:STRING="compiler-rt" )
    else
        args+=( -DCLANG_DEFAULT_RTLIB:STRING="libgcc" )
    fi

    if has_version "dev-lang/llvm:${SLOT}[polly]"; then
        args+=(
            -DWITH_POLLY:BOOL=ON
            -DLINK_POLLY_INTO_TOOLS:BOOL=ON
        )
    fi

    if ! exhost --is-native -q; then
        args+=(
            -DCLANG_TABLEGEN:STRING="${LLVM_PREFIX}/bin/clang-tblgen"
        )
    fi

    cmake_src_configure "${args[@]}"
}

clang_src_compile() {
    ninja_src_compile

    if option doc; then
        edo pushd "${CMAKE_SOURCE}"/docs
        emake -f Makefile.sphinx man html
        edo popd
    fi
}

clang_src_test() {
    PATH="${LLVM_PREFIX}/libexec:${PATH}" \
        CLANG="${PWD}/bin/clang" \
        llvm-project_src_test
}

clang_src_install() {
    cmake_src_install

    # tblgen is not installed by cmake but needed when cross compiling
    exeinto "${LLVM_PREFIX}"/bin
    doexe bin/clang-tblgen

    # CMake gives us:
    # - clang-X.Y
    # - clang -> clang-X.Y
    # - clang++, clang-cl, clang-cpp -> clang
    # We want:
    # - clang-X
    # - clang++-X, clang-cl-X, clang-cpp-X -> clang-X
    # - clang, clang++, clang-cl, clang-cpp -> clang*-X

    edo pushd "${IMAGE}${LLVM_PREFIX}"/bin

    # Symlink versioned clang binaries
    for bin in {clang++,clang-cl,clang-cpp}-${SLOT}; do
        edo ln -s clang-${SLOT} ${bin}
    done

    # Symlink unversioned clang binaries
    for bin in {clang,clang++,clang-cl,clang-cpp}; do
        edo nonfatal rm ${bin}
        edo ln -s ${bin}-${SLOT} ${bin}
    done

    edo popd

    edo pushd "${CMAKE_SOURCE}"

    if option doc; then
        edo pushd docs/_build
        edo cp -t "${IMAGE}${LLVM_PREFIX}"/share/man/man1 man/*
        dodoc -r html
        edo popd
    fi

    edo popd

    if option doc; then
        edo pushd "${IMAGE}${LLVM_PREFIX}"/share/man/man1
        edo ln -s clang.1 clang++.1
        edo ln -s clang.1 clang-cl.1
        edo ln -s clang.1 clang-cpp.1
        dodir "/usr/share/man/man1"
        for bin in {clang,clang++,clang-cl,clang-cpp}; do
            edo ln -s ${bin}.1 ${bin}-${SLOT}.1
            dosym "${LLVM_PREFIX}/share/man/man1/${bin}-${SLOT}.1" "/usr/share/man/man1/${bin}-${SLOT}.1"
        done
        edo popd
    fi

    if option python ; then
        for abi in ${PYTHON_FILTERED_ABIS} ; do
            if option python_abis:${abi} ; then
                insinto $(python_get_sitedir)/clang
                doins "${CMAKE_SOURCE}"/bindings/python/clang/*.py
            fi
        done
        python_bytecompile
    fi

    # Symlinked versioned clang binaries to /usr/$(exhost --target)/bin
    dodir "/usr/$(exhost --target)/bin"
    for bin in {clang,clang++,clang-cl,clang-cpp}-${SLOT}; do
        dosym "${LLVM_PREFIX}/bin/${bin}" "/usr/$(exhost --target)/bin/${bin}"
    done

    # Symlink libclang
    dodir "/usr/$(exhost --target)/lib"
    edo pushd "${IMAGE}${LLVM_PREFIX}"/lib
    for lib in $(ls libclang.so.${SLOT}*); do
        dosym "${LLVM_PREFIX}/lib/${lib}" "/usr/$(exhost --target)/lib/${lib}"
    done
    edo popd

    # We manage alternatives for clang here, and manage alternatives for cc in "app-admin/eclectic-clang"
    local alternatives=()

    # Symlink all clang tools
    edo pushd "${IMAGE}${LLVM_PREFIX}"/bin
    for bin in $(ls); do
        if [[ ${bin} == *-${SLOT} ]]; then
            dosym "${LLVM_PREFIX}/bin/${bin}" "/usr/$(exhost --target)/bin/${bin}"
        else
            alternatives+=("/usr/$(exhost --target)/bin/${bin}" "${LLVM_PREFIX}/bin/${bin}")
        fi
    done
    edo popd

    # Ban unwanted binaries
    for bin in {clang,clang++}; do
        dobanned "${bin}-${SLOT}"
        alternatives+=("${BANNEDDIR}/${bin}" "${bin}-${SLOT}")
    done

    # Symlink manpages
    edo pushd "${IMAGE}${LLVM_PREFIX}"/share/man/man1
    for page in $(ls); do
        if [[ ${page} == *-${SLOT}.1 ]]; then
            dosym "${LLVM_PREFIX}/share/man/man1/${page}" "/usr/share/man/man1/${page}"
        else
            alternatives+=("/usr/share/man/man1/${page}" "${LLVM_PREFIX}/share/man/man1/${page}")
        fi
    done
    edo popd

    # Symlink clang targets
    for bin in {clang,clang-cpp,clang++}; do
        for target in ${CROSS_COMPILE_TARGETS}; do
            if option targets:${target}; then
                edo pushd "${IMAGE}${LLVM_PREFIX}"/bin
                edo ln -s ${bin} ${target}-${bin}
                edo ln -s ${bin}-${SLOT} ${target}-${bin}-${SLOT}
                edo popd
                alternatives+=( /usr/$(exhost --target)/bin/${target}-${bin} ${LLVM_PREFIX}/bin/${target}-${bin} )
                dosym "${LLVM_PREFIX}/bin/${target}-${bin}-${SLOT}" "/usr/$(exhost --target)/bin/${target}-${bin}-${SLOT}"

                if option doc; then
                    edo pushd "${IMAGE}${LLVM_PREFIX}"/share/man/man1
                    edo ln -s ${bin}.1 ${target}-${bin}.1
                    edo ln -s ${bin}-${SLOT}.1 ${target}-${bin}-${SLOT}.1
                    edo popd
                    alternatives+=( /usr/share/man/man1/${target}-${bin}.1 ${LLVM_PREFIX}/share/man/man1/${target}-${bin}.1 )
                    dosym "${LLVM_PREFIX}/share/man/man1/${target}-${bin}-${SLOT}.1" "/usr/share/man/man1/${target}-${bin}-${SLOT}.1"
                fi
            fi
        done
    done

    alternatives_for "clang" "${SLOT}" "${SLOT}" "${alternatives[@]}"
}

