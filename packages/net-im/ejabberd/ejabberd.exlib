# Copyright 2012 Quentin "Sardem FF7" Glidic <sardemff7+exherbo@sardemff7.net>
# Copyright 1999-2015 Gentoo Foundation
# Copyright 2018 Gluzskiy Alexandr <sss@sss.chaoslab.ru>
# Distributed under the terms of the GNU General Public License v2


require github [ user=processone force_git_clone=true ]
require systemd-service
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

export_exlib_phases src_configure src_compile src_install src_test

SUMMARY="A scalable and reliable platform for instant messaging"
DESCRIPTION="
ejabberd is a distributed, fault-tolerant technology that allows the creation of large-scale
instant messaging applications. The server can reliably support thousands of simultaneous users
on a single node and has been designed to provide exceptional standards of fault tolerance. As
an open source technology, based on industry-standards, ejabberd can be used to build bespoke
solutions very cost effectively
"
HOMEPAGE="http://www.process-one.net/en/ejabberd/"

LICENCES="EPL-1.1"
SLOT="0"


MYOPTIONS="
    captcha
    elixir
    ldap
    odbc
    pam
    mysql
    postgresql
    riak
    redis
    latest-deps
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

#        dev-erlang/exmpp


DEPENDENCIES="
    build+run:
        dev-lang/erlang[>=20]
        dev-libs/expat
        dev-libs/libyaml
        sys-libs/zlib
        captcha? (
            media-gfx/ImageMagick[truetype]
            media-libs/gd[webp]
        )
        ldap? ( net-directory/openldap )
        mysql? ( dev-lang/erlang[odbc] )
        odbc? ( dev-db/unixODBC )
        pam? ( sys-libs/pam )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
    run:
        user/ejabberd
        group/ejabberd
        postgresql? ( dev-erlang/pgsql )
        mysql? ( virtual/mysql )
"

#WORK="${WORK}"/src

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --hates=disable-dependency-tracking
    --hates=disable-silent-rules
    --hates=enable-fast-install
    --localstatedir=/var
    --enable-user=ejabberd
    --enable-zlib
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    odbc
    mysql
    "postgresql pgsql"
    pam
    riak
    redis
    elixir
    latest-deps
)

ejabberd_src_configure() {
    esandbox disable_net
    default
    esandbox enable_net
}

ejabberd_src_compile() {
    esandbox disable_net
    default
    esandbox enable_net
}

ejabberd_src_install() {
    default

    keepdir /var/lib/ejabberd
    keepdir /var/log/ejabberd
    edo rmdir "${IMAGE}"/var/lock/{ejabberdctl,}

    install_systemd_files
}

ejabberd_src_test() {
    esandbox disable_net
    default
    esandbox enable_net
}
