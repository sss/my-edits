# Copyright 2008 Richard Brown
# Copyright 2009 Bo Ørsted Andresen
# Copyright 2009 Thomas Anderson
# Copyright 2011 Ali Polatel
# Copyright 2011-2012 Quentin "Sardem FF7" Glidic <sardemff7@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require systemd-service

SUMMARY="A flexible, powerful, server-side application for playing music"
DESCRIPTION="
Music Player Daemon (MPD) is a flexible, powerful, server-side application for playing music.
Through plugins and libraries it can play a variety of sound files while being controlled by
its network protocol.
"
HOMEPAGE="https://www.musicpd.org"
DOWNLOADS="${HOMEPAGE}/download/${PN}/$(ever range -2)/${PNV}.tar.xz"

UPSTREAM_RELEASE_NOTES="http://git.musicpd.org/cgit/master/${PN}.git/plain/NEWS?h=v${PV}"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="aac alsa avahi doc libsamplerate ogg openal pulseaudio sndfile tcpd
    cdio         [[ description = [ CD support through libcdio ] ]]
    curl         [[ description = [ Support obtaining song data via HTTP and enable the WebDAV storage plugin ] ]]
    dsd          [[ description = [ Support for decoding DSD ] ]]
    id3          [[ description = [ Support for ID3 tags ] ]]
    jack         [[ description = [ Enable jack-audio-connection-kit audio output ] ]]
    libmpdclient [[ description = [ Enable support for remote mpd databases ] ]]
    mms          [[ description = [ Microsoft Media Server protocol support ] ]]
    nfs          [[ description = [ Enable support for streaming music over a NFS share ] ]]
    opus         [[ description = [ Opus codec support ] requires = ogg ]]
    samba        [[ description = [ Enable support for streaming music over a SMB share ] ]]
    shout        [[ description = [ Enable support for streaming through shout (mp3, and ogg if ogg is enabled) ] ]]
    soundcloud   [[ description = [ SoundCloud.com support (input) ] ]]
    soxr         [[ description = [ Enable support for the libsoxr resampler ] ]]
    sqlite       [[ description = [ Enable support for storing the MPD database in an Sqlite database ] ]]
    systemd      [[ description = [ systemd socket activation support ] ]]
    upnp         [[ description = [ Support Service Discovery via UPnP ] ]]
    zip          [[ description = [ zip archive support ] ]]
    (
        aac mp3 mikmod musepack wavpack
        audiofile [[ description = [ Enable audiofile support, enables wave support ] ]]
        ffmpeg    [[ description = [ Enable the ffmpeg input plugin, allowing you to play all audio formats supported by ffmpeg/libav ] ]]
        modplug   [[ description = [ mod-like file format support ] ]]
        ( flac shout vorbis ) [[ requires = ogg ]]
    ) [[ number-selected = at-least-one ]]
    ffmpeg? ( ( providers: ffmpeg libav ) [[ number-selected = exactly-one ]] )
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        doc? (
            app-text/docbook-xml-dtd:4.2
            app-text/xmlto
            app-doc/doxygen
        )
    build+run:
        app-arch/bzip2
        dev-libs/boost[>=1.46]
        dev-libs/expat
        dev-libs/glib:2[>=2.28]
        dev-libs/icu:=
        aac? ( media-libs/faad2 )
        alsa? ( sys-sound/alsa-lib[>=0.9.0] )
        audiofile? ( media-libs/audiofile[>=0.3] )
        avahi? ( net-dns/avahi )
        cdio? (
            dev-libs/libcdio
            dev-libs/libcdio-paranoia
        )
        curl? ( net-misc/curl[>=7.18] )
        flac? ( media-libs/flac[>=1.2][ogg?] )
        id3? ( media-libs/libid3tag )
        jack? ( media-sound/jack-audio-connection-kit )
        libmpdclient? ( media-libs/libmpdclient[>=2.2] )
        libsamplerate? ( media-libs/libsamplerate[>=0.1.3] )
        mms? ( media-libs/libmms[>=0.4] )
        mikmod? ( media-libs/libmikmod[>=3.3.6] )
        modplug? ( media-libs/libmodplug )
        mp3? (
            media-libs/libmad
            media-sound/lame
        )
        musepack? ( media-libs/musepack )
        nfs? ( net-fs/libnfs )
        ogg? ( media-libs/libogg )
        openal? ( media-libs/openal )
        opus? ( media-libs/opus )
        ffmpeg? (
            providers:ffmpeg? ( media/ffmpeg )
            providers:libav? ( media/libav )
        )
        pulseaudio? ( media-sound/pulseaudio[>=0.9.16] )
        samba? ( net-fs/samba )
        shout? ( media-libs/libshout )
        sndfile? ( media-libs/libsndfile )
        soundcloud? ( dev-libs/yajl[>=2.0] )
        soxr? ( media-libs/soxr )
        sqlite? ( dev-db/sqlite:3[>=3.7.3] )
        systemd? ( sys-apps/systemd )
        tcpd? ( sys-apps/tcp-wrappers )
        upnp? (
            net-libs/libupnp
        )
        vorbis? ( media-libs/libvorbis )
        wavpack? ( media-sound/wavpack )
        zip? ( dev-libs/zziplib[>=0.13] )
    test:
        dev-cpp/cppunit
    suggestion:
        media-sound/ario       [[ description = [ Provides rhythmbox-like client ] ]]
        media-sound/gmpc       [[ description = [ Provides fast and fully featured GTK-based client ] ]]
        media-sound/mpc        [[ description = [ Provides command line client ] ]]
        media-sound/mpdcron    [[ description = [ Executes scripts based on mpd's idle events ] ]]
        media-sound/ncmpc      [[ description = [ Provides ncurses based command line client ] ]]
        media-sound/pms        [[ description = [ Provides an alternative ncurses based command line client ] ]]
        media-sound/qmpdclient [[ description = [ Provides simple QT client ] ]]
        media-sound/sonata     [[ description = [ Provides an elegant GTK-based client ] ]]
        sys-sound/oss          [[ description = [ Provides an alternative sound architecture instead of ALSA ] ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-bzip2
    --enable-cue
    --enable-daemon
    --enable-database
    --enable-expat
    --enable-fifo
    --enable-httpd-output
    --enable-icu
    --enable-inotify
    --enable-ipv6
    --enable-oss
    --enable-pipe-output
    --enable-recorder-output
    --enable-syslog
    --enable-tcp
    --enable-un
    --enable-wave-encoder
    --disable-adplug
    --disable-ao
    --disable-fluidsynth
    --disable-gme
    --disable-haiku
    --disable-iconv
    --disable-iso9660
    --disable-mpg123
    --disable-roar
    --disable-shine-encoder
    --disable-sidplay
    --disable-sndio
    --disable-twolame-encoder
    --disable-werror
    --disable-wildmidi
    --without-tremor
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    aac
    alsa
    audiofile
    'cdio cdio-paranoia'
    curl
    'curl webdav'
    'doc documentation'
    dsd
    ffmpeg
    flac
    id3
    jack
    libmpdclient
    'libsamplerate lsr'
    mms
    mikmod
    modplug
    'mp3 mad'
    'mp3 lame-encoder'
    'musepack mpc'
    nfs
    vorbis
    'vorbis vorbis-encoder'
    openal
    opus
    'pulseaudio pulse'
    'samba smbclient'
    shout
    sndfile
    soundcloud
    soxr
    sqlite
    'systemd systemd-daemon'
    'tcpd libwrap'
    upnp
    wavpack
    'zip zzip'
)

DEFAULT_SRC_CONFIGURE_TESTS=( '--enable-test --disable-test' )

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'avahi zeroconf avahi'
    "systemd systemdsystemunitdir ${SYSTEMDSYSTEMUNITDIR}"
    "systemd systemduserunitdir ${SYSTEMDUSERUNITDIR}"
)

